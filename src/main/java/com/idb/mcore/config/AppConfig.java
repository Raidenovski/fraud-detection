package com.idb.mcore.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import com.idb.mcore.event.ManagerEventHandler;

@Configuration
@EnableAutoConfiguration
@EnableScheduling
public class AppConfig {

	/**
	 * Handles events when FraudManager is created/updated/deleted
	 * 
	 * @return ManagerEventHandler
	 */
	@Bean
	ManagerEventHandler managerEventHandler() {
		return new ManagerEventHandler();
	}

	/**
	 * Handles REST calls to other servers
	 * 
	 * @param builder
	 * @return RestTemplate
	 */
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
}
