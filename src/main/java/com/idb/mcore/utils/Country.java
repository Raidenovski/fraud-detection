package com.idb.mcore.utils;

public enum Country {

	AU(36, "Australia"), GH(288, "Ghana"), IN(356, "India"), KN(404, "Kenya"), NG(566, "Nigeria"), RU(643,
			"Russia"), ZA(710, "South Africa"), UAE(784, "United Arab Emirates");

	private Integer id;

	private String name;

	private Country(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Loops through all countries and finds a country by it's ID
	 * 
	 * @param countryId
	 * @return Country
	 */
	public static Country getCountryById(Integer countryId) {

		Country result = null;

		for (Country country : Country.values()) {
			if (country.getId().equals(countryId)) {
				result = country;
			}
		}
		return result;
	}
}
