package com.idb.mcore.utils;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Utils {

	private static final JsonParser PARSER = new JsonParser();

	/**
	 * Creates a URL for a checker microservice from the parameters passed as
	 * arguments
	 * 
	 * @param serverIp
	 * @param port
	 * @return String (url)
	 */
	public static String createUrlForChecker(String serverIp, String port) {

		StringBuilder sb = new StringBuilder(serverIp);
		sb.append(port).append("/check");

		return sb.toString();

	}

	/**
	 * Creates Http headers and sents the content type
	 * 
	 * @return
	 */
	public static HttpHeaders createHeadersForPostingJson() {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		return headers;
	}

	/**
	 * Creates an email text body with all the necessary information that is
	 * passed as arguments
	 * 
	 * @param campaignName
	 * @param paramsMap
	 * @param expectedCrPercent
	 * @return String (email text)
	 */
	public static String createEmailTextForCrAlert(String campaignName, Map<String, Object> paramsMap,
			BigDecimal expectedCrPercent) {

		StringBuilder sb = new StringBuilder("High CR Alert!\n");
		sb.append("\n");
		sb.append("Warning!\nCampaign ").append(campaignName).append(" has a CR of ");
		sb.append(paramsMap.get("todayCr")).append("% today\n");
		sb.append("CR on this campaign was ").append(paramsMap.get("yesterdayCr")).append("% yesterday\n");
		sb.append("Diference is ").append(paramsMap.get("gap")).append("%\n");
		sb.append("CR limit on this campaign is ").append(expectedCrPercent).append("%");

		return sb.toString();
	}

	/**
	 * Creates an email text body from the json array that was passed as an
	 * argument for the MultiCampaign alert and the expected parameters that
	 * were set on this alert
	 * 
	 * @param jsonArray
	 * @param jsonExpectedParameters
	 * @return String (email text)
	 */
	public static String createEmailTextForMultiCampaignAlert(JsonArray jsonArray, JsonObject jsonExpectedParameters) {

		StringBuilder sb = new StringBuilder("Multiple Subscription alert!\n");
		sb.append("\n");
		sb.append("Amount of msisdns:	").append("Amount of times subscribed:\n");

		for (JsonElement row : jsonArray) {
			JsonObject jsonRow = PARSER.parse(row.toString()).getAsJsonObject();
			sb.append("\t").append(jsonRow.get("amount_of_msisdns"));
			sb.append("\t\t\t").append(jsonRow.get("amount_of_times_subscribed")).append("\n");
		}

		sb.append("Limit for this alert was set at ").append(jsonExpectedParameters.get("multi_campaign_check_count"));
		sb.append(" campaigns per msisdn in a ").append(jsonExpectedParameters.get("multi_campaign_check_hours"))
				.append(" hour timeframe.");

		String text = sb.toString();
		// remove quotations
		text = text.replaceAll("\"", "");
		return text;
	}

	/**
	 * Creates an email text body from the json array that was passed as an
	 * argument for the IP alert and the expected parameters that were set on
	 * this alert
	 * 
	 * @param jsonArray
	 * @param jsonExpectedParameters
	 * @return String (email text)
	 */
	public static String createEmailTextForIpAlert(JsonArray jsonArray, JsonObject jsonExpectedParameters) {

		StringBuilder sb = new StringBuilder("Multiple Subscription by single IP address alert!\n");
		sb.append("\n");
		sb.append("Amount of IPs:	").append("Amount of times subscribed:\n");

		for (JsonElement row : jsonArray) {
			JsonObject jsonRow = PARSER.parse(row.toString()).getAsJsonObject();
			sb.append("\t\t").append(jsonRow.get("amount_of_ips"));
			sb.append("\t\t\t\t").append(jsonRow.get("amount_of_times_subscribed")).append("\n");
		}
		sb.append("\n");
		sb.append("Limit for this alert was set at ").append(jsonExpectedParameters.get("ip_check_count"));
		sb.append(" campaigns per IP address in ").append(jsonExpectedParameters.get("ip_check_hours"))
				.append(" hours.");

		String text = sb.toString();
		// remove quotations
		text = text.replaceAll("\"", "");
		return text;
	}
}
