package com.idb.mcore.utils;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EmailSender {

	/**
	 * Logs all methods of this class
	 */
	Logger log = LoggerFactory.getLogger(getClass());

	@Value("${email.from}")
	private String from;

	@Value("${email.host}")
	private String host;

	@Value("${email.username}")
	private String user;

	@Value("${email.password}")
	private String password;

	/**
	 * Sends emails to all recepients from the string that is passed as an
	 * argument
	 *
	 * @param emailText
	 * @param emailSubject
	 * @param mailto
	 */
	public String sendEmail(final String emailText, final String emailSubject, final String mailto) {

		final String to = mailto;
		final String[] toS = to.split(",");

		log.info("sendEmail - Start method, sending email to : " + mailto);

		try {
			// When we have several destination mails
			final javax.mail.internet.InternetAddress[] addressTo = new javax.mail.internet.InternetAddress[toS.length];

			for (int i = 0; i < toS.length; i++) {
				addressTo[i] = new javax.mail.internet.InternetAddress(toS[i]);
			}

			// Get system properties
			final Properties mailProperties = System.getProperties();

			// Setup mail server
			mailProperties.setProperty("mail.smtp.host", host);
			mailProperties.setProperty("mail.user", user);
			mailProperties.setProperty("mail.password", password);

			// Get the default Session object.
			final javax.mail.Session sessionMail = javax.mail.Session.getDefaultInstance(mailProperties, null);

			// Create a default MimeMessage object.
			final MimeMessage message = new MimeMessage(sessionMail);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO, addressTo);

			// Set Subject: header field
			message.setSubject(emailSubject);

			// Now set the actual message
			message.setText(emailText);

			// Send message
			Transport.send(message);

			log.info("sendEmail - Email Sent!");

			return "Email Sent!";

		} catch (final Exception e) {
			log.error("sendEmail - ERROR sending mail: " + e.getMessage());
			return "ERROR";
		}
	}
}
