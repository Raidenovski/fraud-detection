package com.idb.mcore.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleAfterDelete;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

import com.idb.mcore.entity.Manager;
import com.idb.mcore.service.ManagerService;

@RepositoryEventHandler
public class ManagerEventHandler {

	@Autowired
	private ManagerService managerService;

	Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * This event is triggered when a Manager is created. The object is passed
	 * as an argument and various checkers are created based on the boolean
	 * fields that the Manager has
	 * 
	 * @param manager
	 */
	@HandleAfterCreate
	public void handleManagerCreateEvent(Manager manager) {

		log.info("AfterCreateEvent - Request: " + manager.toString());

		managerService.handleManagerCreate(manager);
	}

	/**
	 * This event is triggered before a Manager is updated in the database. The
	 * boolean fields are checked and various checker entities are
	 * created/deleted based on the changes in the Manager
	 * 
	 * @param manager
	 */
	@HandleBeforeSave
	public void handleManagerBeforeSaveEvent(Manager manager) {

		log.info("BeforeSaveEvent - Request: " + manager.toString());

		managerService.handleManagerUpdate(manager);
	}

	/**
	 * This event is triggered when a Manager is deleted. All of the checkers
	 * that were active (booleans were true) are deleted along with the manager
	 * 
	 * @param manager
	 */
	@HandleAfterDelete
	public void handleManagerAfterDeleteEvent(Manager manager) {

		log.info("AfterDeleteEvent - Request: " + manager.toString());
		managerService.handleManagerDelete(manager);
	}
}
