package com.idb.mcore.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.idb.mcore.service.ManagerService;

@Component
public class ManagerEventScheduler {

	/**
	 * Main service that handles all caller methods to other services
	 */
	@Autowired
	private ManagerService managerService;

	/**
	 * Logs all methods of this class
	 */
	Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * Scheduled task that executes IP checking by calling appropriate service
	 * methods that send parameters to the microservice in charge of the
	 * execution
	 */
	@Scheduled(cron = "00 00 * * * ? ")
	public void executeIpChecker() {

		log.info("Scheduled task start for IP Checking...");

		managerService.executeChecker("IP");

		log.info("End of IP checking scheduled task!");
	}

	/**
	 * Scheduled task that executes CR checking by calling appropriate service
	 * methods that send parameters to the microservice in charge of the
	 * execution
	 */
	@Scheduled(cron = "00 5 * * * ? ")
	public void executeCrChecker() {

		log.info("Scheduled task start for CR Checking...");

		managerService.executeChecker("CR");

		log.info("End of CR checking scheduled task!");
	}

	/**
	 * Scheduled task that executes CR checking by calling appropriate service
	 * methods that send parameters to the microservice in charge of the
	 * execution
	 */
	@Scheduled(cron = "00 10 * * * ? ")
	public void executeMultiCampaignChecker() {

		log.info("Scheduled task start for MultiCampaign checking...");

		managerService.executeChecker("MC");

		log.info("End of MultiCampaign checking scheduled task!");
	}
}
