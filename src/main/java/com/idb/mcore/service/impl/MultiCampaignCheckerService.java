package com.idb.mcore.service.impl;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.idb.mcore.entity.Manager;
import com.idb.mcore.entity.MultiCampaignChecker;
import com.idb.mcore.repository.MultiCampaignCheckerRepository;
import com.idb.mcore.service.CheckerServiceI;

@Component(value = "multiCampaign")
public class MultiCampaignCheckerService implements CheckerServiceI {

	@Autowired
	private MultiCampaignCheckerRepository mccRepository;

	/**
	 * Logs all methods of this class
	 */
	private Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * Used to parse objects into JSON
	 */
	private Gson GSON = new Gson();
	private JsonParser PARSER = new JsonParser();

	@Value("${json.param.mc.count}")
	private String count;

	@Value("${json.param.mc.hours}")
	private String hours;

	@Override
	public void createChecker(Manager manager, JsonObject jsonParameters) {

		log.info("Creating CR Checker...");
		try {
			MultiCampaignChecker mccChecker = new MultiCampaignChecker();

			// set fields from the manager
			mccChecker.setId(manager.getId());
			mccChecker.setCountry(manager.getCountry());

			// set these field from the JSON parameters
			mccChecker.setCampaignCount(jsonParameters.get(count).getAsInt());
			mccChecker.setRangeOfHours(jsonParameters.get(hours).getAsInt());

			log.info("createChecker - IP Checker created: " + mccChecker.toString());

			// save to DB
			mccRepository.save(mccChecker);

			log.info("createChecker - Checker saved!");

		} catch (Exception e) {
			log.error("createChecker - ERROR creating CrChecker: " + e.getMessage());
		}
	}

	@Override
	public Boolean doesCheckerExist(Long id) {
		log.info("doesCheckerExist - Looking for MultiCampaignChecker...");
		try {
			// call DB to see if exists
			return mccRepository.exists(id);

		} catch (Exception e) {
			log.error("doesCheckerExist - ERROR looking for MultiCampaignChecker: " + e.getMessage());
			return null;
		}
	}

	@Override
	public void deleteCheckerById(Long id) {

		try {
			log.info("deleteCheckerById - Deleting MultiCampaignChecker (ID):" + id);

			// delete from DB
			mccRepository.delete(id);
			log.info("deleteCheckerById - Deleted!");

			// when no results are found
		} catch (EmptyResultDataAccessException erde) {
			log.info("deleteCheckerById - MultiCampaignChecker didn't exist (ID): " + id);
		} catch (Exception e) {
			log.error("deleteCheckerById - ERROR deleting MultiCampaignChecker: " + e.getMessage());
		}

	}

	@Override
	public String getAllCheckerParametersAsJsonArray() {

		try {
			log.info("Getting all MultiCampaign Checkers...");
			ArrayList<MultiCampaignChecker> listOfCheckers = new ArrayList<>();

			mccRepository.findAll().forEach(listOfCheckers::add);

			log.info("Converting to JSON...");

			String jsonArray = GSON.toJson(listOfCheckers);

			return jsonArray;

		} catch (Exception e) {
			log.error("getAllCheckerParametersAsJsonArray - ERROR getting parameters as JSON: " + e.getMessage());
			return null;
		}
	}

}
