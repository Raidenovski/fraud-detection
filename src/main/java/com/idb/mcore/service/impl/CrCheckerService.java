package com.idb.mcore.service.impl;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.idb.mcore.entity.CrChecker;
import com.idb.mcore.entity.Manager;
import com.idb.mcore.repository.CrCheckerRepository;
import com.idb.mcore.service.CheckerServiceI;

@Component(value = "crChecker")
public class CrCheckerService implements CheckerServiceI {

	@Autowired
	private CrCheckerRepository crCheckerRepository;

	/**
	 * Logs all methods of this class
	 */
	private Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * Used to parse objects into JSON
	 */
	private Gson GSON = new Gson();

	@Value("${json.param.cr.percent}")
	private String crCheckPercent;

	@Override
	public void createChecker(Manager manager, JsonObject jsonParameters) {

		log.info("Creating CR Checker...");
		try {
			CrChecker crChecker = new CrChecker();

			// set fields from the manager
			crChecker.setId(manager.getId());
			crChecker.setCountry(manager.getCountry());
			crChecker.setCampaignId(manager.getCampaignId());
			crChecker.setPremiumService(manager.getPremiumService());
			crChecker.setCreated(manager.getCreated());
			crChecker.setUpdated(manager.getUpdated());

			// set this field from the JSON parameters
			crChecker.setCrCheckPercent(jsonParameters.get(crCheckPercent).getAsBigDecimal());

			log.info("createChecker - IP Checker created: " + crChecker.toString());

			// save to DB
			crCheckerRepository.save(crChecker);

			log.info("createChecker - Checker saved!");

		} catch (Exception e) {
			log.error("createChecker - ERROR creating CrChecker: " + e.getMessage());
		}
	}

	@Override
	public Boolean doesCheckerExist(Long id) {

		log.info("doesCheckerExist - Looking for CR Checker...");
		try {
			// call DB to see if exists
			return crCheckerRepository.exists(id);

		} catch (Exception e) {
			log.error("doesCheckerExist - ERROR looking for CrChecker: " + e.getMessage());
			return null;
		}
	}

	@Override
	public void deleteCheckerById(Long id) {

		try {
			log.info("deleteCheckerById - Deleting CR Checker (ID):" + id);

			// delete from DB
			crCheckerRepository.delete(id);
			log.info("deleteCheckerById - Deleted!");

			// when no results are found
		} catch (EmptyResultDataAccessException erde) {
			log.info("deleteCheckerById - CR Checker didn't exist (ID): " + id);
		} catch (Exception e) {
			log.error("deleteCheckerById - ERROR deleting CR checker: " + e.getMessage());
		}

	}

	@Override
	public String getAllCheckerParametersAsJsonArray() {

		try {
			log.info("Getting all CR checkers...");
			ArrayList<CrChecker> listOfCheckers = new ArrayList<>();

			crCheckerRepository.findAll().forEach(listOfCheckers::add);

			log.info("Converting to JSON...");

			String jsonArray = GSON.toJson(listOfCheckers);

			return jsonArray;

		} catch (Exception e) {
			log.error("getAllCheckerParametersAsJsonArray - ERROR getting parameters as JSON: " + e.getMessage());
			return null;
		}
	}

}
