package com.idb.mcore.service.impl;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.idb.mcore.entity.IpChecker;
import com.idb.mcore.entity.Manager;
import com.idb.mcore.repository.IpCheckerRepository;
import com.idb.mcore.service.CheckerServiceI;

@Component(value = "ipChecker")
public class IpCheckerService implements CheckerServiceI {

	/**
	 * Makes CRUD operations for IpChecker entity
	 */
	@Autowired
	private IpCheckerRepository ipCheckerRepository;

	/**
	 * Logs all methods of this class
	 */
	private Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * Used to parse objects into JSON
	 */
	private Gson GSON = new Gson();

	@Value("${json.param.ip.signup_threshhold}")
	private String jsonParamSignupThreshold;

	@Value("${json.param.ip.timeframe}")
	private String jsonParamTimeframe;

	@Override
	public void createChecker(Manager manager, JsonObject jsonParameters) {

		log.info("Creating IP Checker...");
		try {
			IpChecker ipChecker = new IpChecker();

			// set fields from the manager
			ipChecker.setId(manager.getId());
			ipChecker.setCountry(manager.getCountry());
			ipChecker.setCampaignId(manager.getCampaignId());
			ipChecker.setPremiumService(manager.getPremiumService());
			ipChecker.setCreated(manager.getCreated());
			ipChecker.setUpdated(manager.getUpdated());

			// set fields from the JSON parameters
			ipChecker.setSignupThreshold(jsonParameters.get(jsonParamSignupThreshold).getAsInt());
			ipChecker.setTimeframe(jsonParameters.get(jsonParamTimeframe).getAsInt());

			log.info("createChecker - IP Checker created: " + ipChecker.toString());

			// save to DB
			ipCheckerRepository.save(ipChecker);

			log.info("createChecker - Checker saved!");

		} catch (Exception e) {
			log.error("createChecker - ERROR creating IpChecker: " + e.getMessage());
		}

	}

	@Override
	public Boolean doesCheckerExist(Long id) {

		log.info("doesCheckerExist - Looking for IP Checker...");
		try {
			// call DB to see if exists
			return ipCheckerRepository.exists(id);

		} catch (Exception e) {
			log.error("doesCheckerExist - ERROR looking for IpChecker: " + e.getMessage());
			return null;
		}
	}

	@Override
	public void deleteCheckerById(Long id) {

		try {
			log.info("deleteCheckerById - Deleting IP Checker (ID):" + id);

			// delete from DB
			ipCheckerRepository.delete(id);
			log.info("deleteCheckerById - Deleted!");

			// when no results are found
		} catch (EmptyResultDataAccessException erde) {
			log.info("deleteCheckerById - IP Checker didn't exist (ID): " + id);
		} catch (Exception e) {
			log.error("deleteCheckerById - ERROR deleting IP checker: " + e.getMessage());
		}
	}

	@Override
	public String getAllCheckerParametersAsJsonArray() {

		try {
			log.info("Getting all IP checkers...");
			ArrayList<IpChecker> listOfCheckers = new ArrayList<>();

			ipCheckerRepository.findAll().forEach(listOfCheckers::add);

			log.info("Converting to JSON...");

			String jsonArray = GSON.toJson(listOfCheckers);

			return jsonArray;

		} catch (Exception e) {
			log.error("getAllCheckerParametersAsJsonArray - ERROR getting parameters as JSON: " + e.getMessage());
			return null;
		}
	}

}
