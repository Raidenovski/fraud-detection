package com.idb.mcore.service;

import com.google.gson.JsonObject;
import com.idb.mcore.entity.Manager;

public interface CheckerServiceI {

	/**
	 * Creates a checker from the parameters in the json and the Manager object
	 * 
	 * @param manager
	 * @param jsonParameters
	 */
	void createChecker(Manager manager, JsonObject jsonParameters);

	/**
	 * Checks to see if a checker with this ID exists in the database
	 * 
	 * @param id
	 * @return Boolean
	 */
	Boolean doesCheckerExist(Long id);

	/**
	 * Deletes a checker with the corresponding ID
	 * 
	 * @param id
	 */
	void deleteCheckerById(Long id);

	/**
	 * Queries for all checkers of the given type and returns them as an array
	 * in JSON format
	 * 
	 * @return String
	 */
	String getAllCheckerParametersAsJsonArray();
}
