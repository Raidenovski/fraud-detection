package com.idb.mcore.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.idb.mcore.entity.Manager;
import com.idb.mcore.utils.Country;
import com.idb.mcore.utils.EmailSender;
import com.idb.mcore.utils.Utils;

@Service
public class AlertService {

	/**
	 * Manages all checker ids and emails that need to be alerted
	 */
	@Autowired
	private ManagerService managerService;

	/**
	 * Used for sending email alerts
	 */
	@Autowired
	private EmailSender emailSender;

	@Autowired
	private RestTemplate restTemplate;

	/**
	 * Logs all methods of this class
	 */
	private Logger log = LoggerFactory.getLogger(getClass());

	private JsonParser PARSER = new JsonParser();

	@Value("${json.param.cr.percent}")
	private String jsonParamCrPercent;

	/**
	 * Sends an alert that CR checker microservice has triggered. The checker
	 * manager is found by it's ID, email list is taken, text is made to display
	 * the information about the alert and emails are sent
	 * 
	 * @param paramsMap
	 * @return String (response)
	 */
	public String sendCrAlert(Map<String, Object> paramsMap) {

		log.info("Sending CR alert...");
		try {

			// get the checker manager
			Manager manager = managerService.getManager((Long) paramsMap.get("id"));

			// get the expected CR percent parameter from JSON
			JsonObject jsonExpectedParameters = PARSER.parse(manager.getJsonParameters()).getAsJsonObject();
			BigDecimal expectedCrPercent = jsonExpectedParameters.get(jsonParamCrPercent).getAsBigDecimal();

			// get the campaign
			JsonObject jsonCampaign = getCampaignAsJsonObject(manager.getCampaignId());
			String campaignName = jsonCampaign.get("campaignName").getAsString();

			// create subject and text body for the email
			String emailSubject = createEmailSubject(campaignName, "CR");
			String emailText = Utils.createEmailTextForCrAlert(campaignName, paramsMap, expectedCrPercent);

			// send email
			return emailSender.sendEmail(emailText, emailSubject, manager.getAlarmEmails());

		} catch (Exception e) {
			log.info("ERROR sending CR Alert: " + e.getMessage());
			return "ERROR";
		}
	}

	@Value("${json.param.mc.count}")
	private String campaignCount;

	@Value("${json.param.mc.hours}")
	private String hours;

	/**
	 * Handles the process of sending the multi campaign alert. JSON string is
	 * passed as an argument and from it an email is created that is sent to the
	 * addresses indicated in the Manager
	 * 
	 * @param json
	 * @return String (response)
	 */
	public String sendMultiCampaignAlert(String json) {

		log.info("Sending MultiCampaign alert...");

		try {

			// parse JSON
			log.info("Parsing JSON...");
			JsonObject jsonAlert = PARSER.parse(json).getAsJsonObject();

			// get the checker manager and json parameters
			Manager manager = managerService.getManager(jsonAlert.get("managerId").getAsLong());
			JsonObject jsonExpectedParameters = PARSER.parse(manager.getJsonParameters()).getAsJsonObject();

			// get amounts of subscriptions
			String jsonRows = jsonAlert.get("rows").getAsString();
			// remove tabs from JSON
			jsonRows = jsonRows.replace("\t\t", "");
			JsonArray jsonArray = PARSER.parse(jsonRows).getAsJsonArray();

			// get country
			Country country = Country.getCountryById(manager.getCountry());

			// create subject and text body for the email
			String emailSubject = createEmailSubject(country.getName(), "MC");
			String emailText = Utils.createEmailTextForMultiCampaignAlert(jsonArray, jsonExpectedParameters);

			// send email
			return emailSender.sendEmail(emailText, emailSubject, manager.getAlarmEmails());

		} catch (Exception e) {
			log.error("sendMultiCampaignAlert - ERROR sending alert: " + e.getMessage());
			return "ERROR";
		}
	}

	/**
	 * Handles the IP check alert by taking in a json string as an argument and
	 * from it, all necessary information is processed to create an email that
	 * is sent to the addresses indicated in the Manager
	 * 
	 * @param json
	 * @return
	 */
	public String handleIpCheckAlert(String json) {

		log.info("Sending IP alert...");

		try {

			// parse JSON
			log.info("Parsing JSON...");
			JsonObject jsonAlert = PARSER.parse(json).getAsJsonObject();

			// get the checker manager and json parameters
			Manager manager = managerService.getManager(jsonAlert.get("managerId").getAsLong());
			JsonObject jsonExpectedParameters = PARSER.parse(manager.getJsonParameters()).getAsJsonObject();

			// get amounts of subscriptions
			String jsonRows = jsonAlert.get("rows").getAsString();
			JsonArray jsonArray = PARSER.parse(jsonRows).getAsJsonArray();

			// get country
			Country country = Country.getCountryById(manager.getCountry());

			// create subject and text body for the email
			String emailSubject = createEmailSubject(country.getName(), "IP");
			String emailText = Utils.createEmailTextForIpAlert(jsonArray, jsonExpectedParameters);

			// send email
			return emailSender.sendEmail(emailText, emailSubject, manager.getAlarmEmails());

		} catch (Exception e) {
			log.error("sendMultiCampaignAlert - ERROR sending alert: " + e.getMessage());
			return "ERROR";
		}
	}

	/**
	 * Not being used in the current format. Was designed to use with Multiple
	 * campaign alert
	 * 
	 * @param campaignIdList
	 * @return List<String>
	 */
	@Deprecated
	private List<String> getCampaignNames(List<Integer> campaignIdList) {

		log.info("getCampaignNames - Getting campaign names... ");
		List<String> campaignNamesList = new ArrayList<String>();

		try {

			// for each campaign, do following
			for (Integer campaignId : campaignIdList) {

				// get campaign as JSON
				JsonObject jsonCampaign = getCampaignAsJsonObject(campaignId);

				// add the name of the campaign to list
				campaignNamesList.add(jsonCampaign.get("campaignName").getAsString());

			}
			return campaignNamesList;

		} catch (Exception e) {
			log.error("getCampaignNames - ERROR getting campaign names: " + e.getMessage());
			return null;
		}
	}

	/**
	 * Creates a subject for the email based on the type of alert and it's
	 * parameter that is passed as an argument. subjectParameter can be a
	 * campaign name, country name or msisdn
	 * 
	 * @param subjectParameter
	 * @param typeOfAlert
	 * @return String (email subject)
	 */
	private String createEmailSubject(String subjectParameter, String typeOfAlert) {

		// based on type of alert, use text
		String text = null;

		switch (typeOfAlert) {
		case "CR":
			text = "High CR detected on campaign ";
			break;
		case "IP":
			text = "IP addresses having multiple subscriptions in ";
			break;
		case "MC":
			text = "Suspicios activity in ";
			break;
		}

		StringBuilder sb = new StringBuilder(text);
		sb.append(subjectParameter);

		return sb.toString();
	}

	@Value("${ip.rest-server}")
	private String restServer;

	@Value("${ports.campaign}")
	private String campaignPort;

	/**
	 * Takes in the campaign ID as an an argument and returns a JsonObject of
	 * the campaign entity from the Campaign microservice
	 * 
	 * @param campaignId
	 * @return JsonObject
	 */
	private JsonObject getCampaignAsJsonObject(Integer campaignId) {

		StringBuilder sb = new StringBuilder(restServer);
		sb.append(campaignPort).append("/campaign/").append(campaignId);

		String url = sb.toString();

		try {

			log.info("Getting campaign: " + url);

			String response = restTemplate.getForObject(url, String.class);

			JsonObject jsonCampaign = PARSER.parse(response).getAsJsonObject();

			return jsonCampaign;
		} catch (Exception e) {
			log.error("getCampaignAsJsonObject - ERROR obtaining campaign: " + e.getMessage());
			return null;
		}
	}
}
