package com.idb.mcore.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.idb.mcore.entity.Manager;
import com.idb.mcore.repository.ManagerRepository;
import com.idb.mcore.utils.Utils;

@Service
public class ManagerService {

	@Autowired
	@Qualifier("ipChecker")
	private CheckerServiceI ipCheckService;

	@Autowired
	@Qualifier("crChecker")
	private CheckerServiceI crCheckService;

	@Autowired
	@Qualifier("multiCampaign")
	private CheckerServiceI multiCampaignService;

	@Autowired
	private ManagerRepository managerRepository;

	/**
	 * Used for making HTTP REST calls
	 */
	@Autowired
	private RestTemplate restTemplate;

	/**
	 * Parses JSON strings and objects
	 */
	private JsonParser PARSER = new JsonParser();

	/**
	 * Logs all methods of this class
	 */
	private Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * Handles the creation of FraudCheckerManager by checking it's boolean
	 * fields. According to boolean fields set on the manager, other checkers
	 * objects are created
	 * 
	 * @param manager
	 */
	public void handleManagerCreate(Manager manager) {

		log.info("handleManagerCreate - Creating manager...");
		try {

			log.info("handleManagerCreate - Parsing JSON parameters...");
			JsonObject jsonParameters = PARSER.parse(manager.getJsonParameters()).getAsJsonObject();

			// create checkers according to booleans
			if (manager.getIpCheck()) {
				ipCheckService.createChecker(manager, jsonParameters);
			}
			if (manager.getCrCheck()) {
				crCheckService.createChecker(manager, jsonParameters);
			}
			if (manager.getMultiCampaignCheck()) {
				multiCampaignService.createChecker(manager, jsonParameters);
			}

			log.info("handleManagerCreate - End of method!");

		} catch (Exception e) {
			log.error("handleManagerCreate - ERROR creating checkers: " + e.getMessage());
		}
	}

	/**
	 * Handles updating of the FraudCheckerManager by checking if any booleans
	 * were changed. If so, the checkers are created or removed
	 * 
	 * @param manager
	 */
	public void handleManagerUpdate(Manager manager) {

		log.info("handleManagerUpdate - Updating manager (ID): " + manager.getId());

		try {

			// parse JSON for future use of parameters
			log.info("handleManagerCreation - Parsing JSON parameters...");
			JsonObject jsonParameters = PARSER.parse(manager.getJsonParameters()).getAsJsonObject();

			// see if any checkers have changed
			handleIpCheckUpdate(manager, jsonParameters);

			handleCrCheckUpdate(manager, jsonParameters);

			handleMultiCampaignUpdate(manager, jsonParameters);

			log.info("handleManagerUpdate - End of method!");
		} catch (Exception e) {
			log.error("handleManagerUpdate - ERROR Updating checkers: " + e.getMessage());
		}

	}

	/**
	 * Takes in a Manager object and for each active checker, deletes it from
	 * that checkers database by using the ID
	 * 
	 * @param manager
	 */
	public void handleManagerDelete(Manager manager) {

		log.info("handleManagerDelete - Deleting checkers for manager (ID): " + manager.getId());

		// delete all active checkers of this manager
		if (manager.getIpCheck()) {
			ipCheckService.deleteCheckerById(manager.getId());
		}
		if (manager.getCrCheck()) {
			crCheckService.deleteCheckerById(manager.getId());
		}
		if (manager.getMultiCampaignCheck()) {
			multiCampaignService.deleteCheckerById(manager.getId());
		}
		log.info("handleManagerDelete - All active checkers deleted for the manager (ID): " + manager.getId());
	}

	@Value("${ip.server}")
	private String server;

	@Value("${ports.ip_checker}")
	private String ipCheckerPort;

	@Value("${ports.cr_checker}")
	private String crCheckerPort;

	@Value("${ports.mcc_checker}")
	private String mcCheckerPort;

	/**
	 * Depending on the parameter passed, the selected checker is executed by
	 * calling that checker's service to get the parameters and sending all
	 * necessary parameters to the microservice that is in charge of the
	 * checking
	 * 
	 * @param string
	 */
	public void executeChecker(String typeOfCheck) {

		log.info("Executing checker: " + typeOfCheck);
		String arrayOfParameters = null;
		String url = null;

		try {
			// see which checker parameters are needed to be sent
			switch (typeOfCheck) {
			case "IP":
				arrayOfParameters = ipCheckService.getAllCheckerParametersAsJsonArray();
				url = Utils.createUrlForChecker(server, ipCheckerPort);
				break;
			case "CR":
				arrayOfParameters = crCheckService.getAllCheckerParametersAsJsonArray();
				url = Utils.createUrlForChecker(server, crCheckerPort);
				break;
			case "MC":
				arrayOfParameters = multiCampaignService.getAllCheckerParametersAsJsonArray();
				url = Utils.createUrlForChecker(server, mcCheckerPort);
				break;
			default:
				log.error("executeChecker - ERROR! Unknown type of checker: " + typeOfCheck);
			}

			// create headers and an http entity
			HttpHeaders headers = Utils.createHeadersForPostingJson();
			HttpEntity<String> entity = new HttpEntity<String>(arrayOfParameters, headers);

			log.info("executeChecker - Parameters: " + arrayOfParameters);

			// send parameters to the url
			log.info("executeChecker - Sending parameters to: " + url);
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

			// log the response
			log.info("executeChecker - Response: " + response.getBody());

			// occurs when server doesn't respond with a 2xx status
		} catch (HttpStatusCodeException hsce) {
			log.error("executeChecker - ERROR sending parameters to checker: " + hsce.getResponseBodyAsString());
		} catch (Exception e) {
			log.error("executeChecker - ERROR executing checker: " + e.getMessage());
		}
	}

	/**
	 * Finds and returns a Fraud Manager to the caller with the given ID that is
	 * passed as an argument
	 * 
	 * @param id
	 * @return Manager
	 * @throws EmptyResultDataAccessException
	 */
	public Manager getManager(Long id) throws EmptyResultDataAccessException {

		return managerRepository.findOne(id);
	}

	/**
	 * Handles IP checker of the manager when the manager is updated
	 * 
	 * @param manager
	 * @param jsonParameters
	 * @throws Exception
	 */
	private void handleIpCheckUpdate(Manager manager, JsonObject jsonParameters) throws Exception {

		// if this boolean is in the manager
		if (manager.getIpCheck() == true) {

			// see if this checker exists
			Boolean ipCheckExists = ipCheckService.doesCheckerExist(manager.getId());

			if (ipCheckExists) {
				log.info("handleManagerUpdate - IP Checker already exists!");
			} else {
				// if checker doesn't exist, create one
				log.info("handleManagerUpdate - IP Checker does NOT exist! Creating...");
				ipCheckService.createChecker(manager, jsonParameters);
			}

		} else {
			// else, to delete this checker
			log.info("handleManagerUpdate - Manager doesn't have IP check. Deleting IP Checker...");
			ipCheckService.deleteCheckerById(manager.getId());
		}
	}

	/**
	 * Handles CR checker of the manager when the manager is updated
	 * 
	 * @param manager
	 * @param jsonParameters
	 * @throws Exception
	 */
	private void handleCrCheckUpdate(Manager manager, JsonObject jsonParameters) throws Exception {

		// if this boolean is in the manager
		if (manager.getCrCheck() == true) {

			// see if this checker exists
			Boolean crCheckExists = crCheckService.doesCheckerExist(manager.getId());

			if (crCheckExists) {
				log.info("handleManagerUpdate - CR Checker already exists!");

			} else {
				// if checker doesn't exist, create one
				log.info("handleManagerUpdate - CR Checker does NOT exist! Creating...");
				crCheckService.createChecker(manager, jsonParameters);
			}

		} else {
			// else, to delete this checker
			log.info("handleManagerUpdate - Manager doesn't have CR check. Deleting CR Checker...");
			crCheckService.deleteCheckerById(manager.getId());
		}
	}

	/**
	 * Handles MultiCampaign checker of the manager when it is updated
	 * 
	 * @param manager
	 * @param jsonParameters
	 */
	private void handleMultiCampaignUpdate(Manager manager, JsonObject jsonParameters) {

		// if this boolean is in the manager
		if (manager.getMultiCampaignCheck() == true) {

			// see if this checker exists
			Boolean mcCheckExists = multiCampaignService.doesCheckerExist(manager.getId());

			if (mcCheckExists) {
				log.info("handleManagerUpdate - MultiCampaign Checker already exists!");

			} else {
				// if checker doesn't exist, create one
				log.info("handleManagerUpdate - MultiCampaign Checker does NOT exist! Creating...");
				multiCampaignService.createChecker(manager, jsonParameters);
			}

		} else {
			// else, to delete this checker
			log.info(
					"handleManagerUpdate - Manager doesn't have MultiCampaign check. Deleting MultiCampaign Checker...");
			multiCampaignService.deleteCheckerById(manager.getId());
		}

	}
}
