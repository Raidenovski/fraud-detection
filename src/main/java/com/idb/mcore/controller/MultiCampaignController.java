package com.idb.mcore.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.idb.mcore.service.AlertService;

@RestController
public class MultiCampaignController {

	@Autowired
	private AlertService alertService;

	/**
	 * Logs all methods of this class
	 */
	private Logger log = LoggerFactory.getLogger(getClass());

	@PostMapping("/multiCampaignAlert")
	public String getMultiCampaignAlert(@RequestBody String json) {

		log.info("getMultiCampaignAlert - Handle request: " + json);

		return alertService.sendMultiCampaignAlert(json);
	}
}
