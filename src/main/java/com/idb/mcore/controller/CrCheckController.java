package com.idb.mcore.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.idb.mcore.service.AlertService;

@RestController
public class CrCheckController {

	@Autowired
	private AlertService alertService;

	/**
	 * Logs all methods of this class
	 */
	private Logger log = LoggerFactory.getLogger(getClass());

	@GetMapping("/crAlert")
	public String getCrAlert(HttpServletRequest request, @RequestParam(value = "managerId", required = true) Long id,
			@RequestParam(value = "todayCR", required = true) BigDecimal todayCr,
			@RequestParam(value = "yesterdayCR", required = true) BigDecimal yesterdayCr,
			@RequestParam(value = "gap", required = true) BigDecimal gap) {

		log.info("getCrAlert - Handle request: " + request.getQueryString());

		// set all parameters into a map

		Map<String, Object> paramsMap = new HashMap<>();

		paramsMap.put("id", id);
		paramsMap.put("todayCr", todayCr);
		paramsMap.put("yesterdayCr", yesterdayCr);
		paramsMap.put("gap", gap);

		return alertService.sendCrAlert(paramsMap);
	}

}
