package com.idb.mcore.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.idb.mcore.service.AlertService;

@RestController
public class IpCheckController {

	@Autowired
	private AlertService alertService;

	Logger log = LoggerFactory.getLogger(getClass());

	@PostMapping("/ipCheckAlert")
	public String getIpCheckAlert(@RequestBody String json) {

		log.info("getIpCheckAlert - Handle request: " + json);

		return alertService.handleIpCheckAlert(json);
	}
}
