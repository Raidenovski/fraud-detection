package com.idb.mcore.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.idb.mcore.entity.IpChecker;

@Repository
public interface IpCheckerRepository extends CrudRepository<IpChecker, Long> {

}
