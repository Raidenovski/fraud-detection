package com.idb.mcore.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.idb.mcore.entity.CrChecker;

@Repository
public interface CrCheckerRepository extends CrudRepository<CrChecker, Long> {

}
