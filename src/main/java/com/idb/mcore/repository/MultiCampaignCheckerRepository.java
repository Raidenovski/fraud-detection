package com.idb.mcore.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.idb.mcore.entity.MultiCampaignChecker;

@Repository
public interface MultiCampaignCheckerRepository extends CrudRepository<MultiCampaignChecker, Long> {

}
