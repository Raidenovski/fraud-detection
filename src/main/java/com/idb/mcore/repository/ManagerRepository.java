package com.idb.mcore.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.idb.mcore.entity.Manager;

@RepositoryRestResource(collectionResourceRel = "create", path = "create")
public interface ManagerRepository extends CrudRepository<Manager, Long> {
}
